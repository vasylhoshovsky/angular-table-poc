import {Component, OnInit, ViewChild} from '@angular/core';
import {UserService} from '../shared/user/user.service';
import {User} from '../shared/user/user.model';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss']
})
export class UserListComponent implements OnInit{

  page = 1;
  pageSize = 5;
  total: number;
  totalPages: number;

  rowData: any;

  columnDefs = [
    {headerName: 'ID', field: 'id', sortable: true, filter: true, pinned: true, resizable: true, width: 100 },
    {headerName: 'First Name', field: 'first_name', sortable: true, filter: true, resizable: true, width: 500},
    {headerName: 'Last Name', field: 'last_name', sortable: true, filter: true, resizable: true},
    {headerName: 'Email', field: 'email', sortable: true, filter: true, resizable: true}
  ];


  constructor(private userService: UserService) {
  }

  ngOnInit(): void {
    this.fetchData();
  }

  private fetchData() {
    this.userService.getUsers(this.page, this.pageSize).subscribe(e => {
      this.rowData = e.data;
      this.totalPages = e.total_pages;
    });
  }

  onGridReady(params: any) {
    console.log('ready', params);
  }

  setPage(page) {
    this.page = page;
    this.fetchData();
  }

  sortChanged($event: any) {
    console.log($event); // new request after that
  }

  filterChanged($event: any) {
    console.log($event); // new request after that
  }

  columnResized($event: any) {
    console.log($event); // store it
  }

  columnMoved($event: any) {
    console.log($event); // store it
  }

  gridColumnsChanged($event: any) {
    console.log($event); // store it
  }
}
