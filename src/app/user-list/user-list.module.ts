import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { UserListComponent } from './user-list.component';
import {AgGridModule} from 'ag-grid-angular';

@NgModule({
  declarations: [
    UserListComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AgGridModule
  ],
  providers: [],
  exports: [
    UserListComponent
  ],
  bootstrap: [UserListComponent]
})
export class UserListModule { }
