import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root',
})
export class UserService {

  userEndpoint = `https://reqres.in/api/users`;

  constructor(private http: HttpClient) { }

  getUsers(page: number, pageSize: number): any {
    return this.http.get(`${this.userEndpoint}?page=${page}&per_page=${pageSize}`);
  }

}
